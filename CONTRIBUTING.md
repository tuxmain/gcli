# Contributing

Before contributing, please make sure that your development environment is properly configured.

When contributing to this repository, please first discuss the change you wish to make via issue and
via [the technical forum] before making a change.

Please note we have a specific workflow, please follow it in all your interactions with the project.

## Workflow

- You must create an issue for each feature you wish to develop, with a precise title and a
  description of your feature. Then, assign it to yourself and click on the button
  **`Create a merge request`**. GitLab will create a branch dedicated to the issue as well as a
  *Work in Progress* merge request of this branch into the main branch (`master`).
- Please use tags to specify feature domains and concerned modules.
- Write your code according to [project conventions].
- Never contribute to a branch whose issue has not been assigned to you! If the contributor make a
  `git rebase` your commit will be lost !
- Before you push your commit:
  - Document your code.
  - Write unit tests, and verify that they **all** pass.
  - Apply the [project's git conventions]

## Project conventions

- Name your variables and types with long and explicit names. We must understand what is contained in a type or a variable by just reading its name (without looking for the definition or the initialization).
- Variables containing an Option must be suffixed `_opt`.
- Variables containing a Result must be set to `_res`.
- 400 lines per file maximum (excluding tests)
- Cut to the maximum in sub-modules
- When your command provides an interactive view. Separate the view from the logic by creating a struct/enum suffixed `View` that contains all the data to be displayed.
- Test everything that is testable. If your code is not testable, refactor it to extract the logic in pure functions.

## Merge Process

1. Ensure you rebased your branch on the latest `master` commit to avoid any merge conflicts.

2. Ensure that you respect the [commit naming conventions].

3. Ensure that all automated tests pass with the `cargo test --all` command.

4. Push your branch on the gitlab. Briefly explain the purpose of your contribution in the description of the merge request.

5. Tag a ğcli reviewer so he will review your contribution. If you still have no news after several weeks, tag another reviewer or/and talk about your contribution on [the technical forum].

## List of ğcli's reviewers

- @librelois

[the technical forum]: https://forum.duniter.org
[project conventions]: #project-conventions
[project's git conventions]: ./doc/git-conventions.md
[commit naming conventions]: ./doc/git-conventions.md#naming-commits
