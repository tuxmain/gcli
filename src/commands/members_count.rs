//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::*;

pub(crate) fn members_count<W: Write>(client: &Client, out: &mut W) -> anyhow::Result<()> {
    let request_body = MembersCount::build_query(members_count::Variables);

    let members_count::ResponseData {
        current_block: members_count::MembersCountCurrentBlock { members_count },
    } = client.send_gql_query(&request_body)?;

    writeln!(
        out,
        "There is currently {} members in Ğ1 WoT!",
        members_count
    )?;

    Ok(())
}

// below are tests

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_member_count() -> anyhow::Result<()> {
        let mut client = Client::default();
        client
            .expect_send_gql_query::<graphql_client::QueryBody<members_count::Variables>, _>()
            .returning(|_| {
                Ok(members_count::ResponseData {
                    current_block: members_count::MembersCountCurrentBlock {
                        members_count: 10_000,
                    },
                })
            });
        let mut out = Vec::new();
        members_count(&client, &mut out)?;
        let output = std::str::from_utf8(&out)?;

        assert_eq!(output, "There is currently 10000 members in Ğ1 WoT!\n");

        Ok(())
    }
}
