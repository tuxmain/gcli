//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::*;

pub(crate) fn balance<W: Write>(
    client: &Client,
    out: &mut W,
    pubkey_or_script: &str,
    ud_unit: bool,
) -> anyhow::Result<()> {
    let request_body = Balance::build_query(balance::Variables {
        script: pubkey_or_script.to_owned(),
        with_ud: ud_unit,
    });

    let balance::ResponseData {
        balance: balance::BalanceBalance { amount },
        current_ud: current_ud_opt,
    } = client.send_gql_query(&request_body)?;

    if let Some(balance::BalanceCurrentUd { amount: ud_amount }) = current_ud_opt {
        writeln!(
            out,
            "The balance of account '{}' is {:.2} UDĞ1 !",
            pubkey_or_script,
            amount as f64 / ud_amount as f64,
        )?;
    } else {
        writeln!(
            out,
            "The balance of account '{}' is {}.{:02} Ğ1 !",
            pubkey_or_script,
            amount / 100,
            amount % 100
        )?;
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_balance() -> anyhow::Result<()> {
        let mut client = Client::default();
        client
            .expect_send_gql_query::<graphql_client::QueryBody<balance::Variables>, _>()
            .returning(|_| {
                Ok(balance::ResponseData {
                    balance: balance::BalanceBalance { amount: 2_046 },
                    current_ud: None,
                })
            });
        let mut out = Vec::new();
        balance(&client, &mut out, "toto", false)?;
        let output = std::str::from_utf8(&out)?;

        assert_eq!(output, "The balance of account 'toto' is 20.46 Ğ1 !\n");

        Ok(())
    }

    #[test]
    fn test_balance_with_ud_unit() -> anyhow::Result<()> {
        let mut client = Client::default();
        client
            .expect_send_gql_query::<graphql_client::QueryBody<balance::Variables>, _>()
            .returning(|_| {
                Ok(balance::ResponseData {
                    balance: balance::BalanceBalance { amount: 2_046 },
                    current_ud: Some(balance::BalanceCurrentUd { amount: 1_023 }),
                })
            });
        let mut out = Vec::new();
        balance(&client, &mut out, "toto", true)?;
        let output = std::str::from_utf8(&out)?;

        assert_eq!(output, "The balance of account 'toto' is 2.00 UDĞ1 !\n");

        Ok(())
    }
}
