//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#![deny(
    clippy::unwrap_used,
    missing_debug_implementations,
    missing_copy_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unsafe_code,
    unstable_features,
    unused_import_braces
)]

mod client;
mod commands;

#[cfg(not(test))]
use crate::client::Client;
#[cfg(test)]
use crate::client::MockClient as Client;
use crate::commands::Command;
use commands::{balance::balance, current_ud::current_ud, members_count::members_count};
use graphql_client::GraphQLQuery;
use graphql_client::Response;
use std::io::Write;
use std::time::Instant;
use structopt::StructOpt;

const DEFAULT_GVA_SERVER: &str = "https://g1.librelois.fr/gva";

#[derive(Debug, Clone, Copy, GraphQLQuery)]
#[graphql(schema_path = "gql/gva_schema.gql", query_path = "gql/gva_queries.gql")]
pub struct Balance;

#[derive(Debug, Clone, Copy, GraphQLQuery)]
#[graphql(schema_path = "gql/gva_schema.gql", query_path = "gql/gva_queries.gql")]
pub struct CurrentUd;

#[derive(Debug, Clone, Copy, GraphQLQuery)]
#[graphql(schema_path = "gql/gva_schema.gql", query_path = "gql/gva_queries.gql")]
pub struct MembersCount;

#[derive(StructOpt)]
#[structopt(name = "rust-gva-client", about = "Client use GVA API of Duniter.")]
struct CliArgs {
    /// GVA server url
    #[structopt(short, long, default_value = DEFAULT_GVA_SERVER)]
    server: String,
    #[structopt(subcommand)]
    command: Command,
}

fn main() -> anyhow::Result<()> {
    let cli_args = CliArgs::from_args();

    let client = Client::new(cli_args.server);
    let mut out = std::io::stdout();

    match cli_args.command {
        Command::Balance {
            pubkey_or_script,
            ud_unit,
        } => balance(&client, &mut out, &pubkey_or_script, ud_unit)?,
        Command::CurrentUd => current_ud(&client, &mut out)?,
        Command::MembersCount => members_count(&client, &mut out)?,
    }
    Ok(())
}
