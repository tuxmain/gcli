//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::*;

pub(crate) fn current_ud<W: Write>(client: &Client, out: &mut W) -> anyhow::Result<()> {
    let request_body = CurrentUd::build_query(current_ud::Variables);

    if let current_ud::ResponseData {
        current_ud: Some(current_ud::CurrentUdCurrentUd { amount }),
    } = client.send_gql_query(&request_body)?
    {
        let int_part = amount / 100;
        let dec_part = amount % 100;
        writeln!(
            out,
            "The current UD value is {}.{:02} Ğ1 !",
            int_part, dec_part
        )?;
    } else {
        writeln!(out, "server with empty blockchain")?;
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_current_ud() -> anyhow::Result<()> {
        let mut client = Client::default();
        client
            .expect_send_gql_query::<graphql_client::QueryBody<current_ud::Variables>, _>()
            .returning(|_| {
                Ok(current_ud::ResponseData {
                    current_ud: Some(current_ud::CurrentUdCurrentUd { amount: 1_023 }),
                })
            });
        let mut out = Vec::new();
        current_ud(&client, &mut out)?;
        let output = std::str::from_utf8(&out)?;

        assert_eq!(output, "The current UD value is 10.23 Ğ1 !\n");

        Ok(())
    }
}
