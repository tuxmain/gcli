//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::*;

#[cfg_attr(test, allow(dead_code))]
pub(crate) struct Client {
    inner: reqwest::blocking::Client,
    server_url: String,
}

#[cfg_attr(test, mockall::automock, allow(dead_code))]
impl Client {
    pub(crate) fn new(server_url: String) -> Self {
        Client {
            inner: reqwest::blocking::Client::new(),
            server_url,
        }
    }
    pub(crate) fn send_gql_query<
        Req: 'static + serde::Serialize,
        ResData: 'static + serde::de::DeserializeOwned,
    >(
        &self,
        request_body: &Req,
    ) -> anyhow::Result<ResData> {
        let request = self.inner.post(&self.server_url).json(request_body);

        let start_time = Instant::now();
        let response = request.send()?;
        let req_duration = Instant::now().duration_since(start_time);
        println!("The server responded in {} ms.", req_duration.as_millis());

        let mut gql_response: Response<ResData> = response.json()?;
        if let Some(errors) = gql_response.errors.take() {
            print_server_errors(errors);
            Err(anyhow::Error::msg(""))
        } else if let Some(data) = gql_response.data {
            Ok(data)
        } else {
            Err(anyhow::Error::msg("server response contains no data"))
        }
    }
}

fn print_server_errors(errors: Vec<graphql_client::Error>) {
    println!("Server errors:");
    for error in errors {
        println!("{}", error);
    }
}
