# GVA Rust Client

A simple command line client written in Rust that use Duniter GVA API.

## Contribute

Contributions are welcome :)

If you have any questions about the code don't hesitate to ask @elois on the duniter forum: [https://forum.duniter.org](https://forum.duniter.org)

The GraphQL schema is automatically generated from the GVA source code, to update the schema, use the following command:

cargo update -p duniter-gva-gql
