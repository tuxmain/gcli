fn main() {
    let schema_sdl = duniter_gva_gql::get_schema_definition();

    std::fs::write("gql/gva_schema.gql", schema_sdl.as_bytes())
        .expect("Fail to write gva schema in file");
    println!("cargo:rerun-if-changed=build.rs");
}
